# AssignmentGolang



## Getting started

The main project files in **_src/projectapp_**
 

## Installation

Install go on your pc and configure your GOPATH environment variable 
Place this code in your GOPATH

## How to Start Server

Go to src/projectapp directory in terminal

Run- _**go run server.go**_

The webpage will open on **https://www.localhost:80/**

## Features

There are two csv files-
1. **EmployeeCollection.csv**

It contains employee id, user id, designation

2. **UserCollection.csv**

It contains user id, first name, last name, email id

The webpage provides a User interface to :-

- Add a new User to the csv file
- Get a user's details using user id from the csv file
- Update user's email using user id in the csv file



## Working

The server (server.go) and client (javascript) communicate using protobuf and the server subsequently updates the records in the csv files

## TechStack

**CLIENT SIDE**

- Javascript
- Jquery
- HTML
- CSS

**SERVER SIDE**

- Golang
- Protobuf
- Gorilla mux

