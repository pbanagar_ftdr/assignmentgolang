package main

import (
	"fmt"
	"io"
	"strconv"

	//reflect
	"html/template"

	pbUser "projectapp/user"

	"io/ioutil"

	"github.com/gorilla/mux"
	"google.golang.org/protobuf/proto"

	//"log"
	"encoding/base64"
	"encoding/csv"
	"net/http"
	"os"
)

var rout *mux.Router
var templates *template.Template

//var userDetails *pbUser.User
var empReader *csv.Reader
var usrReader *csv.Reader

var empWriter *csv.Writer
var usrWriter *csv.Writer

var empFile *os.File
var usrFile *os.File

// var empRecords [][]string
// var usrRecords [][]string

func init() {
	templates = template.Must(template.ParseGlob("templates/*.html"))
}

func openCsvFiles(mode string) error {
	var err error

	switch mode {
	case "r":
		empFile, err = os.OpenFile("EmployeeCollection.csv", os.O_RDONLY, 0777)
		if err != nil {
			fmt.Println("Employee csv file not found")
			return err

		}
		usrFile, err = os.OpenFile("UserCollection.csv", os.O_RDONLY, 0777)
		if err != nil {
			fmt.Println("User csv file not found")
			return err
		}
		fmt.Println("entered read mode")
		empReader = csv.NewReader(empFile)
		usrReader = csv.NewReader(usrFile)

	case "w":
		empFile, err = os.OpenFile("EmployeeCollection.csv", os.O_WRONLY, 0777)
		if err != nil {
			fmt.Println("Employee csv file not found")
			return err

		}
		usrFile, err = os.OpenFile("UserCollection.csv", os.O_WRONLY, 0777)
		if err != nil {
			fmt.Println("User csv file not found")
			return err
		}
		fmt.Println("entered write mode")
		empWriter = csv.NewWriter(empFile)
		usrWriter = csv.NewWriter(usrFile)

	case "rwa":
		empFile, err = os.OpenFile("EmployeeCollection.csv", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0777)
		if err != nil {
			fmt.Println("Employee csv file not found")
			return err

		}
		usrFile, err = os.OpenFile("UserCollection.csv", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0777)
		if err != nil {
			fmt.Println("User csv file not found")
			return err
		}
		fmt.Println("entered readwrite append mode")
		empReader = csv.NewReader(empFile)
		usrReader = csv.NewReader(usrFile)
		empWriter = csv.NewWriter(empFile)
		usrWriter = csv.NewWriter(usrFile)

	}

	return err
}

func CloseCsvFiles(mode string) {

	if mode == "w" || mode == "rwa" {

		empWriter.Flush()
		usrWriter.Flush()

	}

	empFile.Close()
	usrFile.Close()
}

func Base64Encode(message []byte) []byte {
	b := make([]byte, base64.StdEncoding.EncodedLen(len(message)))
	base64.StdEncoding.Encode(b, message)
	return b
}

func Base64EncodeToString(message []byte) string {

	sEnc := base64.StdEncoding.EncodeToString([]byte(message))

	return sEnc
}

func Base64DecodeFromString(message string) []byte {
	sDec, _ := base64.StdEncoding.DecodeString(message)

	return []byte(sDec)
}

func Base64Decode(message []byte) (b []byte, err error) {
	var l int
	b = make([]byte, base64.StdEncoding.DecodedLen(len(message)))
	l, err = base64.StdEncoding.Decode(b, message)
	if err != nil {
		return
	}
	fmt.Println("b", b)
	return b[:l], nil
}

func homeHandler(w http.ResponseWriter, req *http.Request) {
	//show index.html page
	templates.ExecuteTemplate(w, "index.html", "this is data")
}
func addUser(w http.ResponseWriter, req *http.Request) {
	if err := req.ParseForm(); err != nil {
		fmt.Fprintf(w, "ParseForm() err: %v", err)
		return
	}

	fmt.Println("Post from website! r.PostFrom", req.PostForm)
	//req.FormValue(key string)
	bodyBytes, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return
	}
	fmt.Println("Req", bodyBytes)

	newUser := &pbUser.User{}
	proto.Unmarshal(bodyBytes, newUser)

	fmt.Println(newUser)

	//write to csv
	err = writeNewUserToCsv(newUser)
	if err != nil {
		panic(err)
	}
	w.Write(bodyBytes)

	//fmt.Fprintf(w, "Post from website! r.PostFrom = %v\n", req.PostForm)
}

func updateUser(w http.ResponseWriter, req *http.Request) {
	if err := req.ParseForm(); err != nil {
		fmt.Fprintf(w, "ParseForm() err: %v", err)
		return
	}

	fmt.Println("Post from website! r.PostFrom", req.PostForm)
	//req.FormValue(key string)
	bodyBytes, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return
	}
	fmt.Println("Req", bodyBytes)

	newUser := &pbUser.User{}
	proto.Unmarshal(bodyBytes, newUser)

	fmt.Println(newUser)

	//write to csv
	err = updateNewUserInCsv(newUser)
	if err != nil {
		panic(err)
	}
	w.Write(bodyBytes)

}

func updateNewUserInCsv(newUser *pbUser.User) error {
	var err error
	var usridStr = newUser.GetUserId()
	var usrEmail = newUser.GetEmail()
	var usrRec [][]string

	err = openCsvFiles("r")

	if err != nil {
		return err
	}

	usrRec, err = usrReader.ReadAll()
	if err != nil {
		return err
	}

	CloseCsvFiles("r")
	for i, row := range usrRec {
		if row[0] == usridStr {
			usrRec[i][3] = usrEmail
		}

	}

	err = openCsvFiles("w")
	if err != nil {
		return err
	}
	//now write full

	err = usrWriter.WriteAll(usrRec)
	if err != nil {
		return err
	}

	CloseCsvFiles("w")

	return err
}

func writeNewUserToCsv(newUser *pbUser.User) error {
	fmt.Println("writing to new user")
	var err error
	var empid, usrid int
	var empidStr, usridStr string

	err = openCsvFiles("rwa")
	if err != nil {
		fmt.Println("error in opening csv files")
		return err
	}
	fmt.Println("opened csv files")
	defer CloseCsvFiles("rwa")

	for {
		rec, err := usrReader.Read()

		if err == io.EOF {
			usrid, err = strconv.Atoi(usridStr)
			if err != nil {
				panic(err)
			}
			usrid += 1
			//usridStr = strconv.Itoa(usrid)
			//write to csv

			break
		}

		if err != nil {
			return err
		}

		usridStr = rec[0]

	}

	//writing new user to user csv file
	usridStr = strconv.Itoa(usrid)
	//usrWriter.Write([]string{"\n"})
	//usrWriter.Write(nil)
	usrWriter.Write([]string{usridStr, newUser.GetFirstName(), newUser.GetLastName(), newUser.GetEmail()})

	for {
		rec, err := empReader.Read()
		fmt.Println("emprec", rec)
		if err == io.EOF {
			fmt.Println("empid before eof", empidStr)
			empid, err = strconv.Atoi(empidStr)

			if err != nil {
				return err
			}
			empid += 11
			fmt.Println("empid updated one", empid)
			break
		}
		if err != nil {
			return err
		}

		empidStr = rec[0]

	}

	empidStr = strconv.Itoa(empid)
	//fmt.Println("empid to be written-")
	//empWriter.Write(nil)
	empWriter.Write([]string{empidStr, usridStr, newUser.GetDesignation()})
	fmt.Println("userid", usrid, "empid", empid)

	return err
}

func getUser(w http.ResponseWriter, req *http.Request) {

	useridBase64 := req.URL.Query().Get("proto_body")
	useridProtoBuf := Base64DecodeFromString(useridBase64)

	temp := &pbUser.User{}

	proto.Unmarshal(useridProtoBuf, temp)

	userid := temp.GetUserId()
	fmt.Println("base64code received in query", useridBase64)
	fmt.Println("base64decoded protobuf", useridProtoBuf)
	fmt.Println("protobuf received in query parameter", temp)
	fmt.Println("userid", userid)

	// if err := req.ParseForm(); err != nil {
	// 	panic(err)
	// }
	// for key, value := range req.PostForm {
	// 	fmt.Println(key, value)
	// 	//k = key
	// 	v = value
	// 	//userid, err = strconv.Atoi(value)
	// }
	//vars := mux.Vars(req)
	//userid := vars["userid"]

	// fmt.Println("userid to be found:", userid)
	user, err := retrieveUserDetails(userid)
	if err != nil {
		panic(err)
	}
	var out []byte
	//var err error
	out, err = proto.Marshal(user)
	if err != nil {
		panic(err)
	}
	// fmt.Println("the results- ", out)
	//fmt.Fprint(w, out)
	//templates.ExecuteTemplate(w, "index.html", out)
	//w.Header().Set("Content-Type", "application/octet-stream")
	//fmt.Println("the output to javascript-", out)
	fmt.Println("out", out)
	k, err := w.Write(out)
	fmt.Println(k, err)

	//encode to base64
	b64code := Base64Encode(out)
	fmt.Println("base64 encoded", b64code)

	m, err := Base64Decode(b64code)
	if err != nil {
		panic(err)
	}
	fmt.Println("base64 decoded to bytes ", m)

}

func retrieveUserDetails(userid string) (*pbUser.User, error) {

	var err error
	user := &pbUser.User{}

	err = openCsvFiles("r")
	if err != nil {
		return nil, err
	}
	// var err error
	// empFile, err = os.OpenFile("EmployeeCollection.csv", os.O_RDWR|os.O_APPEND, 0777)
	// if err != nil {
	// 	fmt.Println("Employee csv file not found")
	// }
	// usrFile, err = os.OpenFile("UserCollection.csv", os.O_RDWR|os.O_APPEND, 0777)
	// if err != nil {
	// 	fmt.Println("User csv file not found")
	// }
	//
	// // defer empFile.Close()
	// // defer usrFile.Close()
	//
	// empReader = csv.NewReader(empFile)
	// usrReader = csv.NewReader(usrFile)

	defer CloseCsvFiles("r")

	for {
		rec, err := empReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		if rec[1] == userid {

			user.EmployeeId = rec[0]
			user.UserId = rec[1]
			user.Designation = rec[2]

			break
		}

	}

	for {
		rec, err := usrReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}

		if rec[0] == userid {

			user.FirstName = rec[1]
			user.LastName = rec[2]
			user.Email = rec[3]

			break

		}

	}

	// empRecords, err := empReader.ReadAll()
	// if err != nil {
	// 	panic(err)
	// }
	//
	// usrRecords, err := usrReader.ReadAll()
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Println("empREcords: ", empRecords)
	// fmt.Println("usrRecords: ", usrRecords)
	// for _, row := range empRecords {
	// 	if row[1] == userid {
	//
	// 		user.EmployeeId = row[0]
	// 		user.UserId = row[1]
	// 		user.Designation = row[2]
	//
	// 		break
	// 	}
	// }
	//
	// for _, row := range usrRecords {
	// 	if row[0] == userid {
	//
	// 		user.FirstName = row[1]
	// 		user.LastName = row[2]
	// 		user.Email = row[3]
	//
	// 		break
	//
	// 	}
	// }
	return user, err
}

func main() {

	//
	// out, err := proto.Marshal(user1)
	// fmt.Println(out)
	//
	// a := &pbUser.User{}
	// proto.Unmarshal(out, a)

	rout = mux.NewRouter()

	rout.HandleFunc("/", homeHandler)

	rout.HandleFunc("/assignment/user", getUser).Methods("GET")
	rout.HandleFunc("/assignment/user", addUser).Methods("POST")
	rout.HandleFunc("/assignment/user", updateUser).Methods("PATCH")

	fs := http.FileServer(http.Dir("./assets/"))
	rout.PathPrefix("/assets/").Handler(http.StripPrefix("/assets", fs))

	//rout.HandleFunc("/assignment/user/{userid}", HealthCheckUp).Methods("GET")
	http.Handle("/", rout)
	//fmt.Println("routing has started at 80 port")
	http.ListenAndServe(":80", rout)

}

//Implement a http get api /assignment/user that accepts a userID and returns the user's first name , lastname, Email
//employee id,designation from the csv file

//Implement a http post api /assignment/user request that creates a new user by accepting firstname, lastname, email,
//designation in the request
//return id of newly created user in the response. The newly create user will be saved in the csv file

//Implement a http patch api /assignment/user request that updates the user's email by acceting a userid and email as request
//paramenters

/*
	All 3 apis have the sme uri but different http request methods - this is intentional
	All apis should use protobuf request and response
	For GET request the uri will have a single querystring parameter called proto_body which will have
	base64encoded proto request message.
	The backed will first need to decode3 this message before it can be unmarshalled into a struct

	Use gorilla mux library for implementing http api calls
	Create a private gitlab repo and check-in your code in that repo
	Evaluation will be done based on the checked in code
*/
